
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

public class WordCount {
	public static void main(String[] args) throws FileNotFoundException
	{
		WordCounter wordCount = new WordCounter();
		FileInputStream input = new FileInputStream("src/Alice-in-Wonderland.txt");
		Scanner scanner = new Scanner(input);
		final String DELIMS ="[\\s,.\\?!\"():;]+";
		scanner.useDelimiter( DELIMS );
		while(scanner.hasNext())
		{
			wordCount.addWord(scanner.next().toLowerCase());
		}
		System.out.println(wordCount.getWords());
		for (String string : wordCount.getSortedWords()) {
			System.out.println(string +" : "+ wordCount.getCount(string));
		}
		System.out.println("=======================================================================");
		for (int count = 0;count<21;count++) {
			System.out.println(wordCount.getSortedByValue()[count] +" : "+ wordCount.getCount(wordCount.getSortedByValue()[count]));
		}
		System.out.println(wordCount.getCount("the"));
		scanner.close();
	}
}
